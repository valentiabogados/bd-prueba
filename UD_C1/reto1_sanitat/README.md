# Reto 1: Consultas básicas

> Alumno: Christian Montávez Redondo

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/valentiabogados/bd-prueba/-/tree/main/UD_C1/reto1_sanitat

## Query 1
**Enunciado: Muestre los hospitales existentes (número, nombre y teléfono).**
**Resultado propuesto de modelo:** para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
USE sanitat;
SELECT HOSPITAL_COD,NOM,TELEFON 
FROM HOSPITAL;
```


## Query 2
**Enunciado: muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre.**
**Resultado**: para poder obtener el resultado siempre vamos a mencionar la base de datos a realizar dicha consulta mediante el `USE` seleccionado la bbdd sanitat, posteriormente `SELECT` para indicar en este caso todos los atributos, desde la tabla `HOSPITAL` con el `FROM`, y en el `WHERE` la condición con el atributo `NOM` el `LIKE` y ponemos `_a%` con el objetivo de que nos busque cualquier nombre con cualquier letra delante con el barra baja pero que la segunda letra sea a y el porcentaje para indicar que no tiene límite de cifras detras de la segunda cifra que necesariamente debe ser a:

```sql
USE sanitat;
SELECT *
FROM HOSPITAL
WHERE NOM LIKE'_a%';
```


## Query 3
**Enunciado: muestre los trabajadores (código hospital, código sala, número empleado y apellido) existentes.**
**Resultado:** para poder obtener el resultado siempre vamos a mencionar la base de datos a realizar dicha consulta mediante el `USE` seleccionado la bbdd sanitat, posteriormente `SELECT` para indicar en este caso los atributos `HOSPITAL_COD` `SALA_COD` `EMPLEAT_NO` `COGNOM` y lo usaremos desde `FROM` haciendo uso de la tabla `PLANTILLA`:
```sql
USE sanitat;
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA;
```


## Query 4
**Enunciado: muestre los trabajadores (código hospital, código sala, número empleado y apellido) que no sean del turno de noche.**
**Resultado:** para poder obtener el resultado siempre vamos a mencionar la base de datos a realizar dicha consulta mediante el `USE` seleccionado la bbdd sanitat, posteriormente `SELECT` para indicar en este caso los atributos `HOSPITAL_COD` `SALA_COD` `EMPLEAT_NO` `COGNOM` `FROM` `PLANTILLA`, y para hacer la condición con el `WHERE` usamos el `LIKE` en negación de operación logica con el `NOT` para que el `TORN` no nos salga el horario nocturno:

```sql
USE sanitat;
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA
WHERE TORN NOT LIKE 'N';
```


## Query 5
**Enunciado: muestre a los enfermos nacidos en 1960.**
**Resultado:** para poder obtener el resultado siempre vamos a mencionar la base de datos a realizar dicha consulta mediante el `USE` seleccionado la bbdd sanitat, posteriormente `SELECT` para indicar en este caso todos los atributos, el `FROM` en `MALALT`para buscar en los pacientes y la condición `WHERE` para cumplir con el objetivo de darnos los usuarios nacidos en el año 1960:
```sql
USE sanitat;
SELECT *
FROM MALALT
WHERE YEAR (DATA_NAIX) = 1960;
```


## Query 6
**Enunciado: muestre a los enfermos nacidos a partir del año 1960.**
**Resultado:** para poder obtener el resultado siempre vamos a mencionar la base de datos a realizar dicha consulta mediante el `USE` seleccionado la bbdd sanitat, posteriormente `SELECT` para indicar en este caso todos los atributos, el `FROM` en `MALALT`para buscar en los pacientes y la condición `WHERE` para cumplir con el objetivo de darnos los usuarios nacidos desde el día 01/01/1960 hasta el presente:

```sql
USE sanitat;
SELECT *
FROM MALALT
WHERE DATA_NAIX BETWEEN '1960-01-01' AND current_date();
```

> El presente ejercicio se ha realizado en parte con ayuda de resolución en clase por parte del profesor y resto de alumnos en la pizarra.