-- query 2
USE sanitat;
SELECT *
FROM HOSPITAL
WHERE NOM LIKE'_a%';
-- like significa parecido por eso lo busco
-- para sustituir una letra pongo _ el guión bajo
-- pongo el % y es que tenga las letras que sean

-- Otra posibilidad se hace con:
-- WHERE SUBSTR (NOM, 2, 1)='a';
-- se hace para fijarse en que caracter fijando unos parametros o argumentos y va a devolver un valor