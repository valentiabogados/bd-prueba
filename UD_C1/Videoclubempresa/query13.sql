-- query 13 (eliminamos al actor Xordi y a modo de ejemplo eliminamos a Xavi también)
USE videoclub;
DELETE FROM ACTOR 
WHERE CodiActor = 6 OR CodiActor = 5;
-- En caso de querer eliminar varios actores pondía el CodiActor = 6 OR CodiActor = 5;
-- En caso de eliminar los códigos de actor de varios pOndría el WHERE CodiActor IN (4, 5);