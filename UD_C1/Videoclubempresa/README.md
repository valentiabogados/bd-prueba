# Reto 2: Consultas básicas II

> Alumno: Christian Montávez Redondo

En este reto trabajamos con las bases de datos `empresa`, que nos viene dada en el fichero `empresa.sql` y con `videoclub` que nos viene dada en el fichero `videoclub.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados tanto de `empresa` y de `videoclub`.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/valentiabogados/bd-prueba/-/tree/main/UD_C1/Videoclubempresa

## Query 1
**Enunciado: Muestre los productos (código y descripción) que comercializa la empresa.**
**Resultado**: para resolver la presente consulta en `SELECT` pondremos `PROD_NUM` para mostrar el código y de los productos y `DESCRIPCIO` para mostrar la descripción desde productos.

```sql
-- query 1
USE empresa;
SELECT PROD_NUM, DESCRIPCIO
FROM PRODUCTE;
```

## Query 2
**Enunciado: Muestre los productos (código y descripción) que contienen la palabra tenis en la descripción.**
**Resultado**: para resolver la presente consulta en `WHERE` pondremos `LIKE` para la búsqueda de la palabra tennis.

```sql
-- query 2
USE empresa;
SELECT *
FROM PRODUCTE
WHERE DESCRIPCIO LIKE '%TENNIS%';
```

## Query 3
**Enunciado: Muestre el código, nombre, área y teléfono de los clientes de la empresa.**
**Resultado**: para resolver la presente consulta en el `SELECT` pondremos los campos `CLIENT_COD`, `NOM`, `AREA`, `TELEFON`.

```sql
-- query 3
USE empresa;
SELECT CLIENT_COD, NOM, AREA, TELEFON
FROM CLIENT;
```

## Query 4
**Enunciado: Muestre los clientes (código, nombre, ciudad) que no son del área telefónica 636.**
**Resultado**: para resolver la presente consulta en el `SELECT` pondremos los campos `CLIENT_COD`, `NOM`, `CIUTAT` y en el WHERE tendremos que referirnos al `AREA` distinta al número 636.

```sql
-- query 4
USE empresa;
SELECT CLIENT_COD, NOM, CIUTAT
FROM CLIENT
WHERE AREA != 636;
-- IMPRIMIMOS EN SELECT AREA PARA COMPROBAR QUE LA CONSULTA ESTÉ BIEN (consejo)
```

## Query 5
**Enunciado: Muestre las órdenes de compra de la tabla de pedidos (código, fechas de orden y de envío).**
**Resultado**: para resolver la presente consulta en el `SELECT` pondremos los campos `COM_NUM`, `DATA_TRAMESA`, `COM_DATA` y en el `FROM` haremos sobre la tabla `COMANDA`.

```sql
-- query 5
USE empresa;
SELECT COM_NUM, DATA_TRAMESA, COM_DATA
FROM COMANDA;
```

## Query 6
**Enunciado: Lista de nombres y teléfonos de los clientes.**
**Resultado**: para resolver la presente consulta en el `SELECT` pondremos los campos `NOM`, `TELEFON` y en el `FROM` haremos sobre la tabla `CLIENT`.

```sql
-- query 6
USE videoclub;
SELECT NOM, TELEFON
FROM CLIENT;
```

## Query 7
**Enunciado: Lista de fechas e importes de las facturas**
**Resultado**: para resolver la presente consulta en el `SELECT` pondremos los campos `Data`, `Import` y en el `FROM` haremos sobre la tabla `FACTURA`. Se debe tener en consideración lo explicado por el profesor en clase sobre las palabras reservadas.

```sql
-- query 7
USE videoclub;
SELECT Data, Import
FROM FACTURA;
```

## Query 8
**Enunciado: Lista de productos (descripción) facturados en la factura número 3.**
**Resultado**: para resolver la presente consulta en el `SELECT` pondremos los campos `Descripcio` y en el `FROM` haremos sobre la tabla `DETALLFACTURA` y en el `WHERE` hacemos la condición para que nos de la factura nº 3.

```sql
-- query 8
USE videoclub;
SELECT Descripcio
FROM DETALLFACTURA
WHERE CodiFactura = 3;
```

## Query 9
**Enunciado: Lista de facturas ordenada de forma decreciente por importe.**
**Resultado**: para resolver la presente consulta en el `FROM` haremos sobre la tabla `FACTURA` y añadiremos un `ORDER BY`para la ordenación de los registros seleccionados con el `DESC` para el importe sea descreciente.


```sql
-- query 9
USE videoclub;
SELECT *
FROM FACTURA
ORDER BY Import DESC;
-- Significado de la línea 5: ordenado por importe decreciente
```

## Query 10
**Enunciado: Lista de los actores cuyo nombre comience por X**
**Resultado**: para resolver la presente consulta en el `FROM` haremos sobre la tabla `ACTOR` y añadiremos en `WHERE` el `LIKE` para la búsqueda de la palabra X al inicio del nombre del actor con el fin de obtener aquellos actos con una X en la inicial de su nombre.

```sql
-- query 10
USE videoclub;
SELECT *
FROM ACTOR
WHERE Nom LIKE 'X%';
```

## Query 11
**Enunciado: Añadir dos actores que empiezan por la letra X**
**Resultado**: para resolver la presente consulta tendremos que hacer uso de la función de instertar `INSERT INTO `en la tabla introduciendo el nombre y código del actor.

```sql
-- query 11 (Añado 2 actores que empiezan con la letra X y un tercero con x para la siguiente consulta)
USE videoclub;
INSERT INTO videoclub.ACTOR (Nom ,CodiActor)
VALUES ("Xordi", "6");
-- para añadir otro actor de golpe en la misma línea ("Xordi", "6")
```

## Query 12
**Enunciado: Eliminar al actor "Xordi".**
**Resultado**: para resolver la presente consulta introducimos el `DELETE FROM` haremos sobre la tabla `ACTOR` y añadiremos en `WHERE` el código de actor con el número para poder borrarlo.

```sql
--query 12
DELETE FROM ACTOR AS A
WHERE A.CodiActor = 7;
```

## Query 13
**Enunciado: Elimina varios actores al mismo tiempo**
**Resultado**: para resolver la presente consulta introducimos el `DELETE FROM` haremos sobre la tabla `ACTOR` y añadiremos en `WHERE` el código de actor con la operación lógica OR y de nuevo código de actor con.

```sql
-- query 13 (eliminamos al actor Xordi y a modo de ejemplo eliminamos a Xavi también)
USE videoclub;
DELETE FROM ACTOR 
WHERE CodiActor = 6 OR CodiActor = 5;
-- En caso de querer eliminar varios actores pondía el CodiActor = 6 OR CodiActor = 5;
-- En caso de eliminar los códigos de actor de varios pOndría el WHERE CodiActor IN (4, 5);
```

## Query 14
**Enunciado: Añadir dos actores a la vez.**
**Resultado**: para resolver la presente consulta introducimos el `INSER INTO`con el fin de meter dos actores con su nombre y código de actor y en VALUES introducimos el nombre del actor a crear y su código y a continuación el siguiente a crear.

```sql
-- query 14
INSERT INTO ACTOR (Nom, CodiActor)
VALUES ("Xordi", 6), ("Xavi", 7);

```
## Query 15
**Enunciado: Cambiar en nombre de Xordi por Jordi, o sea, cambiar un registro sin borrarlo.**
**Resultado**: para resolver la presente consulta introducimos el `UPDATE` con el fin de de hacer modificaciones y el `SET` con el cambio a introducir en el `WHERE` fijando el código del actor a modificar.


```sql
-- query 15
USE videoclub;
UPDATE ACTOR
SET Nom = "Jordi"
WHERE CodiActor = 6;
```

## Query 16
**Enunciado: Años de alta de los trabajadores**
**Resultado**: para resolver la presente consulta introducimos en el `SELECT` el `DISTINTC` para que no salgan todos los años repetidos y solamente los años sin repetirse.

```sql
-- query 16
USE empresa;
SELECT DISTINCT YEAR (DATA_ALTA)
FROM EMP;
-- CON DISTINCT ME DA LOS AÑOS DISTINTOS SIN SALIR TODOS LOS AÑOS, SOLAMENTE LOS AÑOS DE REGISTROS DE DATA DE ALTA.
```

## Query 17
**Enunciado: ¿Qué categorias existen actualmente de empleados?**
**Resultado**: para resolver la presente consulta introducimos en el `SELECT` el `DISTINTC` para que no salgan todos los puestos y solamente las categorias sin repetirse.

```sql
-- query 17
USE empresa;
SELECT DISTINCT OFICI
FROM EMP;
```

## Query 18
**Enunciado: ¿Qué empleados van a comisión?**
**Resultado**: para resolver la presente consulta introducimos en el `WHERE` el `IS NOT NULL` con el fin de que se indiquen aquellos empleados que van a comisión, con dicha negación, y ponemos el `ORDER BY` con el objetivo que salga descendiente.
```sql

-- query 18
USE empresa;
SELECT *
FROM EMP
WHERE COMISSIO IS NOT NULL
ORDER BY COMISSIO DESC
-- LAS PONEMOS EN ORDEN DESCENDIENTE
```

## Query 19
**Enunciado: ¿Qué dos empleados se llevan la mayor comisión?**
**Resultado**: para resolver la presente consulta introducimos en el `WHERE` el `IS NOT NULL` con el fin de que se indiquen aquellos empleados que van a comisión, con dicha negación, y ponemos el `ORDER BY` con el objetivo que salga descendiente, ademas añadimos el `LIMIT` de 2 para limitarlo a los dos empleados con mayor comisión.

```sql
-- query 19
USE empresa;
SELECT *
FROM EMP
WHERE COMISSIO IS NOT NULL
ORDER BY COMISSIO DESC
LIMIT 2;
-- LAS PONEMOS EN ORDEN DESCENDIENTE
```
